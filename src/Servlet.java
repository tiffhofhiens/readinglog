import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;



@WebServlet(name = "Servlet", urlPatterns={"/Servlet"})
public class Servlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        UserDAO t = UserDAO.getInstance();

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        String name = request.getParameter("name");
        String id= request.getParameter("id");



//        System.out.println(t.getUser(Integer.parseInt(id)));
//
//        System.out.println("\nUsing a class that converts JSON object to Book object");
//        Book book1 = JSONimport.JSONToBook(t.getUser(Integer.parseInt(id)).getBookItm());
//        System.out.println(book1);

//        System.out.println(t.getUser(224488));
//        System.out.println("\nUsing a class that converts JSON object to Book object");
//        Book book2 = JSONimport.JSONToBook(t.getUser(224488).getBookItm());
//        System.out.println(book2);

        out.println("<html><head> <meta charset=\"UTF-8\">" +
                "    <title>Home</title>" +
                "    <link rel=\"stylesheet\" href=\"normalize.css\">" +
                "    <link rel=\"stylesheet\" href=\"style.css\"></head><body>");
        out.println("<header><h3>Reading Log for " + name + "</h3></header>");
        out.println("<p>ID Number: " + id + "</p>");
        out.println("<h1>Display table of books</h1>");
        out.println("<h3> Title ---- Author ---- Pages </h3>");
        out.println("</body></html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<p>Test GET Response</p>");
    }
}
