import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;

public class JSONimport {

    public static String bookToJSON(Book book) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(book);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Book JSONToBook(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Book book = null;

        try {
            book = mapper.readValue(s, Book.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return book;
    }

}
