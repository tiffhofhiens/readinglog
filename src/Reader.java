public class Reader {

        private String name;
        private int idnumber;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }


        public int getIDnumber() {
            return idnumber;
        }

        public void setIDnumber(int idnumber) {
            this.idnumber = idnumber;
        }

        public String toString() {
            return "Name: " + name + " | Id Number: " + idnumber;
        }
}
