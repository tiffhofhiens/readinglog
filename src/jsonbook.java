import javax.persistence.*;


/** This Data Object class corresponds with customer table
 *  in database. */
@Entity
@Table(name = "jsonbook")
public class jsonbook {

    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idbook")
    private int idbook;


    @Column(name = "json")
    private String json;

    public int getId() {
        return idbook;
    }

    public void setId(int id) {
        this.idbook = id;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String toString() {
        return Integer.toString(idbook) + " " + json;
    }
}