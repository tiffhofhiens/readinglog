import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.List;


/** BookDAO implemented using a singleton pattern
 *  Used to get book data from my MYSQL database*/
public class BookDAO {

    SessionFactory factory = null;
    Session session = null;

    private static BookDAO single_instance = null;

    private BookDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static BookDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new BookDAO();
        }

        return single_instance;
    }

    /** Used to get more than one book from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<jsonbook> getItems() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from jsonbook";
            List<jsonbook> cs = (List<jsonbook>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single book from database */
    public jsonbook getItem(int idbook) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from jsonbook where idbook=" + Integer.toString(idbook);
            jsonbook c = (jsonbook) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}

