import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Book {


  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "idbook")
  private double id;

  @Column(name = "title")
  private String title;

  @Column(name = "subtitle")
  private String subtitle;

  @Column(name = "author")
  private String author;

  @Column(name = "pages")
  private double pages;


public double getId() { return id;}

public void setId(double id) {this.id = id;}

public String getTitle(){
  return title;
}

public void setTitle(String title) {
  this.title = title;
}

public String getSubtitle() { return subtitle; }

public void setSubtitle(String subtitle) { this.subtitle = subtitle; }

public String getAuthor() {
  return author;
}

public void setAuthor(String author) {
  this.author = author;
}

public double getPages() {
  return pages;
}

public void setPages(double pages) {
  this.pages = pages;
}

public String toString() {
  if (subtitle==null) {
    return "Id: " + id + " | Title: " + title + " | Author: " + author +" | Pages: " + pages;
  }
  else{
    return "Id: " + id +  " | Title: " + title +" "+ subtitle + " | Author: " + author +" | Pages: " + pages;
  }

}
}
