
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static boolean checkName( String name ) {
        return name.matches( "[A-Z][a-z]*" );
    }

    public static void main(String[] args) throws MalformedURLException {

        BookDAO t = BookDAO.getInstance();
        Scanner input = new Scanner(System.in);

        List<Book> bookList = new ArrayList<>();
        List<User> userList = new ArrayList<>();


        List<jsonbook> c = t.getItems();
        int count = 1;
//        System.out.println("Print List JSON");
        for (jsonbook i : c) {
            Book newBook = JSONimport.JSONToBook(t.getItem(count).getJson());
//            System.out.println(i);
            bookList.add(newBook);
            count++;
        }

        userList.add(new User(112233 ,"Jordan" ,bookList.subList(2,5)));
        userList.add(new User(101010 ,"Tiffany",bookList.subList(10,15)));


        String login;
        String userName;
        boolean validName = false;
        boolean validID = false;
        boolean found = false;
        int idNumber = 0;
        int length;

        System.out.println(" ---------------------------------------------------------");
        System.out.println(" ------------ Welcome to your Reading List----------------");
        System.out.println(" ---------------------------------------------------------");
        System.out.println(" ---        Would you like to Login or Sign up?        ---\n");
        System.out.print("Press L for Login S for Sign up:");
        login = input.next();

            do {
                System.out.print("Please enter your name:");
                userName = input.next();
                if(login.equals("L")){
                    for (User u: userList){
                        if (u.getName().equals(userName)) {
                            validName = true;
                            found = true;
                            break;
                        }
                    }
                }
                else if (checkName(userName)) {
                    validName = true;
                } else {
                    System.out.println("Please enter first name (example: Justin");
                    validName = false;
                }
            } while (!(validName));

            do {
                System.out.print("Please enter a 6-digit ID number:");
                if (input.hasNextInt()) {
                    idNumber = input.nextInt();
                    length = String.valueOf(idNumber).length();
                    if (length == 6) {
                        validID = true;
                    } else {
                        System.out.println("must be 6 digits");
                        validID = false;
                    }
                } else {
                    System.out.println("only use numbers [0-9]");
                    validID = false;
                    input.next();
                }
                if(login.equals("L")){
                    for (User u: userList){
                        if (u.getId() == idNumber) {
                            validID = true;
                            break;
                        }
                    }
                }
            } while (!(validID));
            if(login.equals("S") || !found){
                userList.add(new User(idNumber ,userName));
                System.out.println("new user created");

            }

        System.out.println("\n---------------------------------------");
        System.out.println("\n           Welcome "+ userName +"      ");
        System.out.println("\n---------------------------------------");
        System.out.println("\n--------   Your Book List   -----------");
        List<Book> temp = null;
        for(User u: userList){
            if (userName.equals(u.getName())){
                temp = u.getMyBooks();
                for (Book book : temp) {
                    System.out.println(book);
                }
            }
        }

        String action = "Y";
        System.out.println("Would you like to add to your reading list? ");
        System.out.print("Please enter Y or N: ");
        action = input.next();
        if (action.equals("Y")){
            System.out.println("Print Book Library");
            for (Book value : bookList) {
                System.out.println(value);
            }
            double nbook;
            System.out.println("Would you like to update your list?");
            System.out.println("Select book by id ");
            nbook = input.nextDouble();
            for (Book b: bookList){
                if( nbook == b.getId()){
                    for(User u: userList){
                        if (userName.equals(u.getName())){
                            temp = u.getMyBooks();
                            for (Book book : temp) {
                                System.out.println(book);
                            }
                        }
                    }
                }
            }
            /** Currently all new books must be added in the database first*/
//            String nbook;
//            System.out.println("Please enter your book in the following format (Json Object): ");
//            System.out.println("{\"id\": 17, \"title\": \"Eragon\", \"subtitle\": \"\", \"author\": \"Christopher Paolini\", \"pages\": 382} ");
//            nbook = input.next();
//            Book newBook = JSONimport.JSONToBook(nbook);
//            System.out.println(nbook);
//            bookList.add(newBook);
//            temp.add(newBook);
        }else{
            System.out.println("Have a great day, Goodbye");
        }

    }
}


