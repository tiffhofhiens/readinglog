
import java.util.ArrayList;
import java.util.List;

public class User {

    private int id;
    private String name;
    private List<Book> myBooks = new ArrayList<>();

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public User(int id, String name, List<Book> myBooks) {
        this.id = id;
        this.name = name;
        this.myBooks = myBooks;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() { return name;}

    public void setName(String name) { this.name = name;}

    public List<Book> getMyBooks() {
        return myBooks;
    }

    public void setMyBooks(List<Book> myBooks) {
        this.myBooks = myBooks;
    }

    public String toString() {
        return Integer.toString(id) + " " + name;
    }
}