import junit.framework.TestCase;
import org.junit.Test;

public class JSONimportTest extends TestCase {

    String s = "{\"id\": 1, \"title\": \"Harrpy Potter\", \"subtitle\": \"and Soccerer's Stone\", \"author\": \"J.K. Rowling\", \"pages\": 472}";

    @Test
    public void testJSONToBook() {
        Book b = JSONimport.JSONToBook(s);
        assertEquals(b.getId(), 1.0);
    }

}