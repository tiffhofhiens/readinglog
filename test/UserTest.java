import junit.framework.TestCase;
import org.junit.Test;

public class UserTest extends TestCase {


    User u = new User(102030,"Sample");
    @Test
    public void testGetId() {
        assertEquals(u.getId(),102030);
    }
    @Test
    public void testSetId() {
        u.setId(232323);
        assertEquals(u.getId(),232323);
    }
    @Test
    public void testTestGetName() {
        assertEquals(u.getName(),"Sample");
    }
    @Test
    public void testTestSetName() {
        u.setName("NewName");
        assertEquals(u.getName(),"NewName");
    }

}