import junit.framework.TestCase;
import org.junit.Test;

import java.util.List;

public class BookDAOTest extends TestCase {

    BookDAO t = BookDAO.getInstance();
//    List<String> temp;
    List<jsonbook> temp = t.getItems();

    @Test
    public void testGetItems() {
        assertEquals("There are 16 objects in the list", temp.size(), 16 );
    }
    @Test
    public void testGetItem() {
        String test = (t.getItem(2).getJson());
        assertTrue(test!= null);
    }
}